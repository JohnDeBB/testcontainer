//
//  RTEViewFacory.h
//  TestContainer
//
//  Created by shupeng on 6/6/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "viewFactoryProtoco.h"

@interface RTEViewFacory : NSObject <viewFactoryProtoco>

@end
