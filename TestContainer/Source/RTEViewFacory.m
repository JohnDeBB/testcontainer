//
//  RTEViewFacory.m
//  TestContainer
//
//  Created by shupeng on 6/6/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "RTEViewFacory.h"
#import "RichTextEditor.h"

@implementation RTEViewFacory

- (NSString *)getName
{
    return @"RTF Editor";
}
- (UIView *)createViewWithConfiguration:(NSString *)configuration
{
    if (configuration) {
        return [[RichTextEditor alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    }
    else{
        return [[RichTextEditor alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    }
}

- (void)releaseView:(UIView *)view
{
    [view removeFromSuperview];
}

- (NSArray *)getConfigurationArray
{
    return nil;
}

@end
