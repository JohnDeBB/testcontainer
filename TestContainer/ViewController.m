//
//  ViewController.m
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "ViewController.h"
#import "PopoverView/PopoverView.h"
#import "PickView.h"
#import "TestWrapperView.h"
#import "rootViewFactory.h"
#import "viewFactoryProtoco.h"

@interface ViewController () <PopoverViewDelegate, PickViewDelegate, TestWrapperViewDelegate>
{
    rootViewFactory *_rootVF;
    NSArray *_viewFactories;
    NSMutableArray *_viewFactoryStringArray;
    UIButton *_controllBtn;
    
    PopoverView *_popView;
    
    UIView *_canvas;
    
}
@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.title = @"Test";
    // left
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBtnPressed:)];
    _controllBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    [_controllBtn setTitle:@"Controll Model" forState:UIControlStateNormal];
    [_controllBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_controllBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    [_controllBtn addTarget:self action:@selector(controllBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_controllBtn sizeToFit];
    UIBarButtonItem *controllBtn = [[UIBarButtonItem alloc] initWithCustomView:_controllBtn];
    self.navigationItem.leftBarButtonItems = @[addBtn, controllBtn];
    
    // right
    UIBarButtonItem *clearBtn = [[UIBarButtonItem alloc] initWithTitle:@"clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearBtnPressed:)];
    self.navigationItem.rightBarButtonItems = @[clearBtn];
    
    _canvas = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_canvas];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupViewFactory];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupViewFactory
{
    _rootVF = [rootViewFactory new];
    
    _viewFactories = [_rootVF getAllFactory];
    _viewFactoryStringArray = [NSMutableArray array];
    
    for (id <viewFactoryProtoco>view in _viewFactories) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
        NSString *key = [view getName];
        NSArray *allConfiguration = [view getConfigurationArray];
        if (allConfiguration.count) {
            [dic setObject:allConfiguration forKey:key];
        }
        else{
            [dic setObject:[NSArray array] forKey:key];
        }
        
        [_viewFactoryStringArray addObject:dic];
    }
}

- (void)enableControllMode
{
    for (TestWrapperView *view in _canvas.subviews) {
        [view setControllState:YES];
    }
}

- (void)disableControllMode
{
    for (TestWrapperView *view in _canvas.subviews) {
        [view setControllState:NO];
    }
}

#pragma mark - Local Action
- (void)addBtnPressed:(id)sender
{
    PickView *pv = [[PickView alloc] initWithFrame:CGRectMake(0, 0, 200, 200) withMenuItms:_viewFactoryStringArray];
    pv.delegate = self;
    
    _popView = [PopoverView showPopoverAtPoint:CGPointMake(20, 64) inView:self.view withTitle:nil withContentView:pv delegate:self];
}

- (void)controllBtnPressed:(UIButton *)sender
{
    if (sender.selected) {
        [UIView animateWithDuration:3 animations:^{
            sender.selected = NO;
        }];
        [self disableControllMode];
    }
    else{
        sender.selected = YES;
        [self enableControllMode];
    }
}

- (void)clearBtnPressed:(id)sender
{
    for (TestWrapperView *view in _canvas.subviews) {
        [view removeFromSuperview];
    }
}

#pragma makr - PV Delegate
- (void)pickView:(PickView *)pv didSelectAtIndexPath:(NSIndexPath *)indexPath
{
    static NSInteger count = 0;
    count++;

    [_popView dismiss:YES];
    
    id <viewFactoryProtoco> factory = [_viewFactories objectAtIndex:indexPath.section];
    
    NSString *title = [[[_viewFactoryStringArray objectAtIndex:indexPath.section] allKeys] firstObject];
    NSString *configuration;
    if ([[[[_viewFactoryStringArray objectAtIndex:indexPath.section] allValues] firstObject] count]) {
        configuration = [[[_viewFactoryStringArray objectAtIndex:indexPath.section] allValues] firstObject];
    }
    UIView *view = [factory createViewWithConfiguration:configuration];
    CGRect frame = view.frame;
    frame.origin.x = 10 + count;
    frame.origin.y = 70 + count;
    view.frame = frame;
    
    TestWrapperView *wrapperView = [[TestWrapperView alloc] initWithContentView:view title:[NSString stringWithFormat:@"%@:%@",title, configuration]];
    wrapperView.delegate = self;
    
    [wrapperView setControllState:_controllBtn.selected];
    [_canvas addSubview:wrapperView];
    
}

#pragma mark - Text WrapperView delegate
- (void)testWrapperViewDidBeginControl:(TestWrapperView *)view
{
    [_canvas bringSubviewToFront:view];
}
@end
