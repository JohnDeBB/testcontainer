//
//  TestWrapperView.m
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "TestWrapperView.h"

@interface TestWrapperView () <UIGestureRecognizerDelegate>
{
    NSString *_title;
    
    
    UIView *_overlay;
    UILabel *_tipLabel;
    
    UIView *_contentView;
    
    // pan state
    CGPoint _center;
    CGSize _originSize;
    
    NSInteger _panMode;
}
@end


@implementation TestWrapperView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithContentView:(UIView *)contentView title:(NSString *)title
{
    if (self = [super initWithFrame:CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width + 2, contentView.frame.size.height + 2)]) {
        _title = title;
        
        _contentView = contentView;
        _contentView.frame = CGRectMake(1, 1, _contentView.frame.size.width, _contentView.frame.size.height);
        _contentView.autoresizingMask = _contentView.autoresizingMask | (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        [self addSubview:_contentView];
        
        _overlay = [[UIView alloc] initWithFrame:self.bounds];
        _overlay.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        _overlay.userInteractionEnabled = NO;
        [self addSubview:_overlay];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [_overlay addGestureRecognizer:pan];
        
        _tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
        _tipLabel.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        _tipLabel.font = [UIFont systemFontOfSize:12];
        _tipLabel.textColor = [UIColor whiteColor];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
        _tipLabel.numberOfLines = 0;
        _tipLabel.backgroundColor = [UIColor colorWithWhite:.2 alpha:.8];
        _tipLabel.hidden = YES;
        [_overlay addSubview:_tipLabel];
        
    }

    
    
    return self;
}


- (void)handlePan:(UIPanGestureRecognizer *)ges
{
    switch (ges.state) {
        case UIGestureRecognizerStateBegan:
            if (_panMode == 0) {
                
            }
            else{
                _center = self.center;
                _originSize = self.bounds.size;
                _tipLabel.hidden = NO;
            }

            if ([self.delegate respondsToSelector:@selector(testWrapperViewDidBeginControl:)]) {
                [self.delegate testWrapperViewDidBeginControl:self];
            }
            break;
            
        case UIGestureRecognizerStateChanged:
        {
            CGPoint transition = [ges translationInView:self];
            
            
            if (_panMode == 0) {
                self.center = CGPointMake(self.center.x + transition.x, self.center.y + transition.y);
            }
            else{
                CGRect frame = self.frame;
                frame.size.width += transition.x * 2;
                frame.size.height += transition.y * 2;
                self.frame = frame;
                self.center = _center;
                
                
                _tipLabel.text = [NSString stringWithFormat:@"%@\n(%.0f,%.0f)",_title,frame.size.width,frame.size.height];
                _tipLabel.center = CGPointMake(frame.size.width/2, frame.size.height/2);
            }
            
            
            [ges setTranslation:CGPointZero inView:self];


        }
            break;
            
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
        {
            _tipLabel.hidden = YES;
        }
            break;
        default:
            break;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:self];
    if (point.x >= .6f*self.bounds.size.width && point.y >= .6f*self.bounds.size.height) {
        _panMode = 1;
    }
    else{
        _panMode = 0;
    }
    
    return YES;
}

- (void)setControllState:(BOOL)state
{
    _overlay.userInteractionEnabled = state;
    _contentView.userInteractionEnabled = !state;
    
//    if (state) {
//        self.backgroundColor = [UIColor redColor];
//        [UIView animateWithDuration:100.f animations:^{
//            self.alpha = .5f;
//            [UIView setAnimationRepeatCount:NSIntegerMax];
//            [UIView setAnimationRepeatAutoreverses:YES];
//        } completion:^(BOOL finished) {
//            
//        }];
//    }
//    else{
//        
//    }
}
@end
