//
//  TestWrapperView.h
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TestWrapperView;
@protocol TestWrapperViewDelegate <NSObject>
- (void)testWrapperViewDidBeginControl:(TestWrapperView *)view;
@end

@interface TestWrapperView : UIView

- (id)initWithContentView:(UIView *)contentView title:(NSString *)title;
- (void)setControllState:(BOOL)state;
@property id<TestWrapperViewDelegate> delegate;
@end


