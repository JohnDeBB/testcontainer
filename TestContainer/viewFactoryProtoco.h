//
//  viewFactoryProtoco.h
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol viewFactoryProtoco <NSObject>

@required
- (NSString *)getName;
- (UIView *)createViewWithConfiguration:(NSString *)configuration;
- (void)releaseView:(UIView *)view;
- (NSArray *)getConfigurationArray;
@end
