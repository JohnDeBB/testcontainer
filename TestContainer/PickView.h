//
//  PickView.h
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PickView;
@protocol PickViewDelegate <NSObject>
- (void)pickView:(PickView *)pv didSelectAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface PickView : UIView

/*
 items: @[@{key: @[]}, @{key: @[]},...]
 */
- (id)initWithFrame:(CGRect)frame withMenuItms:(NSArray *)items;
@property (weak, nonatomic) id<PickViewDelegate> delegate;
@end



