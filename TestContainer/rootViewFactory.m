//
//  rootViewFactory.m
//  TestContainer
//
//  Created by shupeng on 6/6/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "rootViewFactory.h"



/*
 sub factory
 */
#import "RTEViewFacory.h"


@implementation rootViewFactory

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (NSArray *)getAllFactory
{
    NSMutableArray *allFactories = [NSMutableArray array];
    
    RTEViewFacory *rteF = [RTEViewFacory new];
    [allFactories addObject:rteF];
    
    return [NSArray arrayWithArray:allFactories];
}
@end
