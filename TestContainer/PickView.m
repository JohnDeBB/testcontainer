//
//  PickView.m
//  TestContainer
//
//  Created by shupeng on 6/5/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "PickView.h"

@interface PickView () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_items;

    
    UIScrollView *_scrollView;
    UITableView *_tableView1;
    UITableView *_tableView2;
    
    NSInteger _table1SelectedIndex;
}
@end


@implementation PickView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withMenuItms:(NSArray *)items
{
    if (self = [super initWithFrame:frame]) {
        _items = items;
        
        [self setupTableView];
    }
    return self;
}

- (void)setupTableView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.scrollEnabled = NO;
    _scrollView.clipsToBounds = YES;
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.contentSize = CGSizeMake(_scrollView.bounds.size.width * 2, _scrollView.bounds.size.height);
    [self addSubview:_scrollView];
    
    _tableView1 = [[UITableView alloc] initWithFrame:_scrollView.bounds style:UITableViewStylePlain];
    _tableView1.dataSource = self;
    _tableView1.delegate = self;
    _tableView1.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:_tableView1];
    
    CGRect frame = _scrollView.bounds;
    frame.origin.x += frame.size.width;
    _tableView2 = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView2.dataSource = self;
    _tableView2.delegate = self;
    _tableView2.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:_tableView2];
    
    _table1SelectedIndex = NSIntegerMax;
}


#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView1) {
        return _items.count;
    }
    else{
        if (_table1SelectedIndex != NSIntegerMax) {
            return [[[[_items objectAtIndex:_table1SelectedIndex] allValues] firstObject] count];
        }
        else{
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (tableView == _tableView1) {
        NSString *title = [[[_items objectAtIndex:indexPath.row] allKeys] firstObject];
        cell.textLabel.text = title;
        
        NSArray *configurationArray = [[[_items objectAtIndex:indexPath.row] allValues] firstObject];
        if (configurationArray.count == 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    else{
        NSString *title = [[[[_items objectAtIndex:_table1SelectedIndex] allValues] firstObject] objectAtIndex:indexPath.row];
        cell.textLabel.text = title;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _tableView1) {
        _table1SelectedIndex = indexPath.row;
        
        NSArray *configurationArray = [[[_items objectAtIndex:indexPath.row] allValues] firstObject];
        if (configurationArray.count == 0) {
            [self.delegate pickView:self didSelectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.row]];
        }
        else{
            [_tableView2 reloadData];
            [_scrollView setContentOffset:CGPointMake(_scrollView.bounds.size.width, 0) animated:YES];
        }
    }
    else{
        if ([self.delegate respondsToSelector:@selector(pickView:didSelectAtIndexPath:)]) {
            [self.delegate pickView:self didSelectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:_table1SelectedIndex]];
        }
    }
}

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    if (newWindow != nil) {
        [_scrollView setContentOffset:CGPointZero animated:NO];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
@end
